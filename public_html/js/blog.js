/* global Handlebars */

$(function () {
    var APPLICATION_ID = "0A4008E9-20DB-9389-FF7A-92DDE87BDA00",
        SECRET_KEY =  "F3F3C09A-68FE-0DEB-FFD8-D6A482840F00",
        VERSION = "v1";
        
    Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
    
    var postsCollection = Backendless.Persistence.of(Posts).find();
    
    console.log(postsCollection);
    
    var wrapper = {
        posts: postsCollection.data
    };
    
    
    Handlebars.registerHelper('format', function (time) {
        return moment(time).format("");
    });
    var blogScript = $("#blogs-template").html();
    var blogTemplate = Handlebars.compile(blogScript);
    var blogHTML = blogTemplate(wrapper);
    
    $('.main-container').html(blogHTML);

});

function Posts(args){
    args = args || "";
    this.title = args.title || "";
    this.content = args.conetent || "";
    this.authorEmail = args.authorEmail || "";
}
